Night at the Museum by Xavi Heras

Unity version: 2017.1.0p4
Google VR SDK version: 1.60.0
Platform: Android
Test device: Pixel XL

Please check medium.com article to see a version with a complete video walkthrough and pictures here:

https://medium.com/@xavier.heras.divison/night-at-the-museum-udacity-vr-developer-nanodegree-1177c37bc61



Introduction

	For the fifth Udacity VR Developer Nanodegree project I had to prepare a “museum” experience. This brief postmortem shows the initial objectives and describes the included features.

Objectives

	The objective of this project was to investigate about an industry that has the potential to be impacted by VR. I decided to investigate about healthcare, a sector I truly believe is already using VR to improve people lives.

	I wanted to build a very accesible experience for everyone and I had a couple of design factors very clear from the begining. First, users should have the possibility to move around the museum freely. Second, after some tests I decided to do not use blocks of text to avoid eye strain. 

	To accomplish this I decided to use a gaze locomotion system and show information using only videos and pictures. All 3d models will be modeled using blender. Finally to create a more intimate experience the app should be able to dynamically change the mood for each scene (lights and sounds).

Break down of the final experience

	Main menu and tutorial

		User starts the experience in front of main menu. This area was designed to focus on head movement and locomotion events. As soon the user turn around a small tutorial shows how to navigate through the environment. 

		Navigability was a priority from the beginng. First iterations used museum floor as a “walk-able” area. This solution was not good because users could move to close to objects or even enter inside undesired places breaking presence. To fix this a red carpet was implemented to guide and limit users movement. This carpet helped to avoid collisions and limit accesible areas, improving the feeling of presence.


	Entrance and museum central hub

		Once users knows how to move around the scene they can enter the museum. A big sign introduces museum theme and shows the entrance to the main hall.

		From the main hall users can access to each thematic “station”, five in total. The hexagonal shape of this hub was designed with the intention to guide users to move around easily and see clearly all stations.

	Stations

		Each “station” match thematically with the presented content using 3d models, sounds, effects, lights and videos. To make the experience more accesible, videos starts automatically when the user enters the “station” and pauses when leaving. Some pictures add more information to the experience and each station mood is improved with specific sounds and effects (particles, animations, etc).

			- Fears and phobias (elevator scene): A stormy night from an elevator after a blackout.

			- Post-Traumatic Stress Disorder (war zone tent scene): A subtle night war scene from inside a tent with distant combat and helicopter sounds.

			- Alleviate Anxiety for Pediatric Patients (whale underwater scene): Breathtaking scene from inside an underwater sea cave with a huge whale.

			- Pain and Stress Control (zen house and garden scene): A relaxing night view of a zen garden with bright stars from inside a traditional japanese house.

			- Elderly and Dementia (old room memory scene): A black and white scene (from an almost lost memory) of a room with a big armchair in front of a fireplace with an old gramophone.


Conclusion

I enjoyed a lot this project but I am not totally satisfied because some unexpected performance issues forced me to change a lot my original design. I had less than 10 days to finish this project (around 50 hours) and I lost most of the time trying to improve performance (video issues) and baking lights. Anyways after researching about VR applied to healthcare I really would like to work someday in this field.


Video walkthrough: https://www.youtube.com/watch?v=mXHscmCfr-4
