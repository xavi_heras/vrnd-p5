﻿using UnityEngine;
using System.Collections;

public class RaycastMovement : MonoBehaviour {
	public GameObject raycastHolder;
	public GameObject player;
	public GameObject raycastIndicator;
	public GameObject reticle;

	public LayerMask layerMask;

	public float height = 4;
	public bool teleport = true;

	public AudioSource source;
	public AudioClip clip;

	public float maxMoveDistance = 10;
	
	private bool moving = false;

	RaycastHit hit;
	float theDistance;

	// Use this for initialization
	void Start () {
	
	}

	// Update is called once per frame
	void Update () {
		
		Vector3 forwardDir = raycastHolder.transform.TransformDirection (Vector3.forward) * 100;
		//Debug.DrawRay (raycastHolder.transform.position, forwardDir, Color.green);

		if (Physics.Raycast (raycastHolder.transform.position, (forwardDir), out hit, layerMask)) {

			if (hit.collider.gameObject.tag == "movementCapable") {
				ManageIndicator ();
				if (hit.distance <= maxMoveDistance) { //If we are close enough

					//If the indicator isn't active already make it active.
					EnableIndicator ();
				
					if (Input.GetMouseButtonDown (0)) {
						if (teleport) {
							teleportMove (hit.point);
						} else {
							DashMove (hit.point);
						}
					}
				} else {
					DisableIndicator ();
				}
			} else {
				DisableIndicator ();
			}
				
		} else {
			DisableIndicator ();
		}
	
	}

	private void EnableIndicator(){
		if (raycastIndicator.activeSelf == false) {
			raycastIndicator.SetActive (true);

		}
		reticle.SetActive (false);
	}

	private void DisableIndicator(){
		if (raycastIndicator.activeSelf == true) {
			raycastIndicator.SetActive (false);

		}
		reticle.SetActive (true);
	}

	public void ManageIndicator() {
		if (!teleport) {
			if (moving != true) {
				raycastIndicator.transform.position = hit.point;
			}
			if(Vector3.Distance(raycastIndicator.transform.position, player.transform.position) <= 4.5) {
				moving = false;
			}

		} else {
			raycastIndicator.transform.position = hit.point;
		}
	}
	public void DashMove(Vector3 location) {
		moving = true;

		source.PlayOneShot (clip);

		iTween.MoveTo (player, 
			iTween.Hash (
				"position", new Vector3 (location.x, location.y + height, location.z), 
				"time", .2F, 
				"easetype", "linear"
			)
		);
	}
	public void teleportMove(Vector3 location) {
		player.transform.position = new Vector3 (location.x, location.y + height, location.z);
	}
}
