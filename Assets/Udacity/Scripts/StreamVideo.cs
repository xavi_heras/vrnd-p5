﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;


public class StreamVideo : MonoBehaviour {

    public RawImage image;
    public GameObject playIcon;

    public VideoClip videoToPlay;

    private VideoPlayer videoPlayer;
    private VideoSource videoSource;

    private AudioSource audioSource;

    private bool isPaused = false;
    private bool firstRun = true;

	void Start (){
		//PlayPause ();
		image.enabled = false;
	}

    IEnumerator playVideo() {
        playIcon.SetActive(false);
        firstRun = false;

        //add videoPlayer
        videoPlayer = gameObject.AddComponent<VideoPlayer>();

        //get AudioSource
		audioSource = GetComponent<AudioSource>();

        //disable Play on awake
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;
        audioSource.Pause();

        //play from video clip
        videoPlayer.source = VideoSource.VideoClip;
		        
        //set audio 
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        //set video 
        videoPlayer.clip = videoToPlay;
        videoPlayer.Prepare();

        //wait video is ready
        while (!videoPlayer.isPrepared) {
            yield return null;
        }
			        	
        //assign texture to be displayed
		image.enabled = true;
		image.texture = videoPlayer.texture;

        //play Video and sound
        videoPlayer.Play();
        audioSource.Play();

        while (videoPlayer.isPlaying) {            
            yield return null;
        }

    }

    public void PlayPause() {
        if(!firstRun && !isPaused) {
            videoPlayer.Pause();
            audioSource.Pause();
            playIcon.SetActive(true);
            isPaused = true;
			image.enabled = false;
        } else if (!firstRun && isPaused) {
            videoPlayer.Play();
            audioSource.Play();
            playIcon.SetActive(false);
            isPaused = false;
			image.enabled = true;
        } else {
            StartCoroutine(playVideo());
        }
    }


}
