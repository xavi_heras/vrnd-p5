﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class FogBackground : MonoBehaviour {

	public Color fogColor;
	public float fogStart;
	public float fogEnd;
	public RawImage fakeBackground = null;

	public float tweenTime = .5f;

	public Color cameraColor;

	public bool videoAutoplay = true;
	public GameObject video;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other){
		//Debug.Log(other.name + " entered!");



		for (int i=0; i< Camera.allCamerasCount ; i++){
			Camera.allCameras [i].clearFlags = CameraClearFlags.Color;
			//Camera.allCameras [i].backgroundColor = cameraColor;
			DOTween.To (() => Camera.allCameras [i].backgroundColor , x => Camera.allCameras [i].backgroundColor  = x, cameraColor, tweenTime);
		}

		DOTween.To (() => RenderSettings.fogColor, x => RenderSettings.fogColor = x, fogColor, tweenTime);
		DOTween.To (() => RenderSettings.fogStartDistance, x => RenderSettings.fogStartDistance= x, fogStart, tweenTime);
		DOTween.To (() => RenderSettings.fogEndDistance, x => RenderSettings.fogEndDistance= x, fogEnd, tweenTime);


		//RenderSettings.fogColor = fogColor;
		//RenderSettings.fogStartDistance = fogStart;
		//RenderSettings.fogEndDistance = fogEnd;
		//RenderSettings.fog = true;

		if (fakeBackground!=null) DOTween.ToAlpha (() => fakeBackground.color, x => fakeBackground.color = x, 0 , tweenTime*3);

		if (videoAutoplay) {
			if (video != null) {
				video.GetComponent<StreamVideo> ().PlayPause ();
			}
		}
	}

	void OnTriggerExit(Collider other){
		//Debug.Log(other.name + " exited!");
		for (int i=0; i< Camera.allCamerasCount ; i++){			
			Camera.allCameras [i].clearFlags = CameraClearFlags.Skybox;
		}


		//RenderSettings.fog = false;
		//RenderSettings.fogColor = Color.white;
		//RenderSettings.fogStartDistance = 50f;
		//RenderSettings.fogEndDistance = 200f;

		if (fakeBackground != null) {
			DOTween.KillAll ();
			fakeBackground.color = new Color (fakeBackground.color.r,fakeBackground.color.g,fakeBackground.color.b,1f);
		}

		DOTween.To (() => RenderSettings.fogColor, x => RenderSettings.fogColor = x, Color.white, tweenTime);
		DOTween.To (() => RenderSettings.fogStartDistance, x => RenderSettings.fogStartDistance= x, 50f, tweenTime);
		DOTween.To (() => RenderSettings.fogEndDistance, x => RenderSettings.fogEndDistance= x, 200f, tweenTime);

		if (videoAutoplay) {
			if (video != null) {
				video.GetComponent<StreamVideo> ().PlayPause ();
			}
		}

	}


}
